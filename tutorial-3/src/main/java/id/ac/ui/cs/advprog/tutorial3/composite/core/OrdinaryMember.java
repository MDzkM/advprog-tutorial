package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        String name, role;
        List<Member> childMembersList;

        public OrdinaryMember(String name, String role) {
                this.name = name;
                this.role = role;
                childMembersList = new ArrayList<>();
        }

        public String getName() {
            return this.name;
        }

        public String getRole() {
                return this.role;
        }

        public void addChildMember(Member member) {
                return;
        }

        public void removeChildMember(Member member) {
                return;
        }

        public List<Member> getChildMembers() {
                return this.childMembersList;
        }
}
