package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        String name, role;
        List<Member> childMembersList;

        public PremiumMember(String name, String role) {
                this.name = name;
                this.role = role;
                childMembersList = new ArrayList<>();
        }

        public String getName() {
                return this.name;
        }

        public String getRole() {
                return this.role;
        }

        public void addChildMember(Member member) {
                if (childMembersList.size() == 3 && !this.role.equals("Master")) {
                        return;
                }
                childMembersList.add(member);
        }

        public void removeChildMember(Member member) {
                childMembersList.remove(member);
        }

        public List<Member> getChildMembers() {
                return this.childMembersList;
        }
}
