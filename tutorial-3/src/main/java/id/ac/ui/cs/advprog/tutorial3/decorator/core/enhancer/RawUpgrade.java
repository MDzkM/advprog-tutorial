package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    Random seed;
    int enhancedValue;

    public RawUpgrade(Weapon weapon) {
        this.weapon= weapon;
        this.seed = new Random();
        this.enhancedValue = 5 + seed.nextInt(5);
    }

    @Override
    public String getName() {
        return this.weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        return this.weapon.getWeaponValue() + enhancedValue;
    }

    @Override
    public String getDescription() {
        return this.weapon.getDescription() + " This weapon is imbued with raw power.";
    }
}
