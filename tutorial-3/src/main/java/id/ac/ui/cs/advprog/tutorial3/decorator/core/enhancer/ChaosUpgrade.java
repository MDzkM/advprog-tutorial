package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    Random seed;
    int enhancedValue;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon= weapon;
        this.seed = new Random();
        this.enhancedValue = 50 + seed.nextInt(5);
    }

    @Override
    public String getName() {
        return this.weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return this.weapon.getWeaponValue() + enhancedValue;
    }

    @Override
    public String getDescription() {
        return this.weapon.getDescription() + " This weapon is imbued with chaos.";
    }
}
