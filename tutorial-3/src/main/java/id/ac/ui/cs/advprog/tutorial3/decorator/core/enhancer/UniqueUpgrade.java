package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    Random seed;
    int enhancedValue;

    public UniqueUpgrade(Weapon weapon) {
        this.weapon = weapon;
        this.seed = new Random();
        this.enhancedValue = 10 + seed.nextInt(5);
    }

    @Override
    public String getName() {
        return this.weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        return this.weapon.getWeaponValue() + enhancedValue;
    }

    @Override
    public String getDescription() {
        return this.weapon.getDescription() + " This weapon is imbued with unique powers.";
    }
}
