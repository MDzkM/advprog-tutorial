package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        int childCountBefore = member.getChildMembers().size();
        Member memberPremium = new PremiumMember("Premium Member", "Premium");
        member.addChildMember(memberPremium);
        assertEquals(childCountBefore + 1, member.getChildMembers().size());
        
        childCountBefore = member.getChildMembers().size();
        Member memberOrdinary = new OrdinaryMember("Ordinary Member", "Ordinary");
        member.addChildMember(memberOrdinary);
        assertEquals(childCountBefore + 1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member memberPremium = new PremiumMember("Premium Member", "Premium");
        member.addChildMember(memberPremium);
        int childCountBefore = member.getChildMembers().size();
        member.removeChildMember(memberPremium);
        assertEquals(childCountBefore - 1, member.getChildMembers().size());
        
        Member memberOrdinary = new OrdinaryMember("Ordinary Member", "Ordinary");
        member.addChildMember(memberOrdinary);
        childCountBefore = member.getChildMembers().size();
        member.removeChildMember(memberOrdinary);
        assertEquals(childCountBefore - 1, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member memberPremium1 = new PremiumMember("Premium Member 1", "Premium");
        Member memberPremium2 = new PremiumMember("Premium Member 2", "Premium");
        Member memberPremium3 = new PremiumMember("Premium Member 3", "Premium");
        Member memberPremium4 = new PremiumMember("Premium Member 4", "Premium");

        member.addChildMember(memberPremium1);
        assertEquals(1, member.getChildMembers().size());
        member.addChildMember(memberPremium2);
        assertEquals(2, member.getChildMembers().size());
        member.addChildMember(memberPremium3);
        assertEquals(3, member.getChildMembers().size());
        member.addChildMember(memberPremium4);
        assertEquals(3, member.getChildMembers().size());
        member.removeChildMember(memberPremium1);
        member.removeChildMember(memberPremium2);
        member.removeChildMember(memberPremium3);

        
        Member memberOrdinary1 = new OrdinaryMember("Ordinary Member 1", "Ordinary");
        Member memberOrdinary2 = new OrdinaryMember("Ordinary Member 2", "Ordinary");
        Member memberOrdinary3 = new OrdinaryMember("Ordinary Member 3", "Ordinary");
        Member memberOrdinary4 = new OrdinaryMember("Ordinary Member 4", "Ordinary");

        member.addChildMember(memberOrdinary1);
        assertEquals(1, member.getChildMembers().size());
        member.addChildMember(memberOrdinary2);
        assertEquals(2, member.getChildMembers().size());
        member.addChildMember(memberOrdinary3);
        assertEquals(3, member.getChildMembers().size());
        member.addChildMember(memberOrdinary4);
        assertEquals(3, member.getChildMembers().size());
        member.removeChildMember(memberOrdinary1);
        member.removeChildMember(memberOrdinary2);
        member.removeChildMember(memberOrdinary3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member guildMaster = new PremiumMember("Guild Master", "Master");
        
        Member memberPremium1 = new PremiumMember("Premium Member 1", "Premium");
        Member memberPremium2 = new PremiumMember("Premium Member 2", "Premium");
        Member memberPremium3 = new PremiumMember("Premium Member 3", "Premium");
        Member memberPremium4 = new PremiumMember("Premium Member 4", "Premium");

        guildMaster.addChildMember(memberPremium1);
        assertEquals(1, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberPremium2);
        assertEquals(2, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberPremium3);
        assertEquals(3, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberPremium4);
        assertEquals(4, guildMaster.getChildMembers().size());
        guildMaster.removeChildMember(memberPremium1);
        guildMaster.removeChildMember(memberPremium2);
        guildMaster.removeChildMember(memberPremium3);
        guildMaster.removeChildMember(memberPremium4);

        
        Member memberOrdinary1 = new OrdinaryMember("Ordinary Member 1", "Ordinary");
        Member memberOrdinary2 = new OrdinaryMember("Ordinary Member 2", "Ordinary");
        Member memberOrdinary3 = new OrdinaryMember("Ordinary Member 3", "Ordinary");
        Member memberOrdinary4 = new OrdinaryMember("Ordinary Member 4", "Ordinary");

        guildMaster.addChildMember(memberOrdinary1);
        assertEquals(1, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberOrdinary2);
        assertEquals(2, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberOrdinary3);
        assertEquals(3, guildMaster.getChildMembers().size());
        guildMaster.addChildMember(memberOrdinary4);
        assertEquals(4, guildMaster.getChildMembers().size());
        guildMaster.removeChildMember(memberOrdinary1);
        guildMaster.removeChildMember(memberOrdinary2);
        guildMaster.removeChildMember(memberOrdinary3);
        guildMaster.removeChildMember(memberOrdinary4);
    }
}
