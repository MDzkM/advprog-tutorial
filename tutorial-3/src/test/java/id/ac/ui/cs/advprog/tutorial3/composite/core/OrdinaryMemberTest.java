package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Asuna", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Waifu", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member childOrdinary = new OrdinaryMember("Ordinary Child", "Child");
        member.addChildMember(childOrdinary);
        assertEquals(0, member.getChildMembers().size());

        Member childPremium = new PremiumMember("Premium Child", "Child");
        member.addChildMember(childPremium);
        assertEquals(0, member.getChildMembers().size());
    }
}
